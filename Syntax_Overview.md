# Syntax Overview

```kei
# This is a comment
{# This is a multiline commment
{# nested comment #}
#}

# Let binding
let my_var : Uint64 = 42
# Sugar when type inference is possible
my_var := 42

```
