# Kei lang

Kei is a project of a new programing language. This book is at the moment a collection
of design ideas and possible directions of the language.

## Language characteristics

- Strongly typed (with ADT)
- Object oriented
- Interpreted and compiled
  - Easy REPL
  - The compilation process may use the interpreter to generate the final static code (Metaclasses)
- Fast and usable exception system
- Safe parallelism with actors and reference ownership semantics
- Reference counting garbage collector

The language feels like python, with brackets for blocks, strict typing and all classes are pydantic models. It also
adds a few features like:
 - almost everything is an expression
 - a block gets the value (and type) of it's last expression
 - Rust-style enum
 - match
 - real anonymous functions

## Technical stack

The reference implementation is in Rust.

The target platform is Wasm, the interpreter is a rust application which embeds the generated
wasm using [wasmer](https://wasmer.io)
